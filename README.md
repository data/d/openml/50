# OpenML dataset: tic-tac-toe

https://www.openml.org/d/50

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: David W. Aha    
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Tic-Tac-Toe+Endgame) - 1991   
**Please cite**: [UCI](http://archive.ics.uci.edu/ml/citation_policy.html)

**Tic-Tac-Toe Endgame database**  
This database encodes the complete set of possible board configurations at the end of tic-tac-toe games, where "x" is assumed to have played first.  The target concept is "win for x" (i.e., true when "x" has one of 8 possible ways to create a "three-in-a-row").  

### Attribute Information  

     (x=player x has taken, o=player o has taken, b=blank)
     1. top-left-square: {x,o,b}
     2. top-middle-square: {x,o,b}
     3. top-right-square: {x,o,b}
     4. middle-left-square: {x,o,b}
     5. middle-middle-square: {x,o,b}
     6. middle-right-square: {x,o,b}
     7. bottom-left-square: {x,o,b}
     8. bottom-middle-square: {x,o,b}
     9. bottom-right-square: {x,o,b}
    10. Class: {positive,negative}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/50) of an [OpenML dataset](https://www.openml.org/d/50). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/50/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/50/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/50/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

